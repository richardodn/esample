﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel;
using System.Threading;
using System.Threading.Tasks;

namespace grepClientForms
{
    [CallbackBehavior(
        ConcurrencyMode = ConcurrencyMode.Single,
        UseSynchronizationContext = false)]
    public class GrepServiceCallback : ServiceReference.IDocumentGrepServiceCallback
    {

        public GrepServiceCallback(MainForm form)
        {
            _form = form;
            _uiContext = SynchronizationContext.Current;
        }
        private SynchronizationContext _uiContext;
        private MainForm _form;

        public void OnSearchCompletedCallback(string documentPath, string searchString, Guid clientInstance, int count)
        {
            // set the count subitem of the 
            System.Diagnostics.Debug.WriteLine(string.Format("OnSearchCompletedCallback({0}, {1}, {2}, {3})", documentPath, searchString, clientInstance.ToString(), count));
            _uiContext.Post(_form.PostResultsToUI, new SearchResult(documentPath, searchString, clientInstance, count));
        }
    }
}
