﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.ServiceModel;

namespace grepClientForms
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            // size the columns
            listSearchItems_Resize(sender, e);

            // set enabled states
            txtSearch_TextChanged(sender, e);

            // Contruct the service objects. Be aware the service may not be running and this will throw an exception.
            try
            {
                // connect to the service while registering callback
                _callback = new GrepServiceCallback(this);
                _instanceContext = new InstanceContext(_callback);
                _client = new ServiceReference.DocumentGrepServiceClient(_instanceContext);

                _client.Subscribe();
            }
            catch (EndpointNotFoundException ex)
            {
                MessageBox.Show("An exception was thrown while connecting to the Document Grep Service. Has the service been started?");
#if DEBUG
                MessageBox.Show(ex.StackTrace);
#endif
                Close();
            }
        }

        protected override void OnClosing(CancelEventArgs e)
        {
            try
            {
                _client.Unsubscribe();
            }
            catch(Exception)
            {
                // Nothing we can do now
            }

            base.OnClosing(e);
        }

        public void PostResultsToUI(object state)
        {
            SearchResult result = state as SearchResult;
            if (chkMonitorOnly.Checked)
            {
                if (!_guidToPath.ContainsKey(result.Instance))
                {
                    AddSearchItem(result.Path, result.Instance);
                }

            }

            if (_guidToPath.ContainsKey(result.Instance)) // may be from another process
            {
                ListViewItem item = listSearchItems.FindItemWithText(_guidToPath[result.Instance]);
                if (!ReferenceEquals(item, null))
                {
                    if (item.SubItems.Count == 1)
                    {
                        // add one
                        item.SubItems.Add(result.Result.ToString());
                    }
                    else
                    {
                        // update
                        item.SubItems[1].Text = result.Result.ToString();
                    }
                }
            }
        }

        public void PostEnableStartButton(object state)
        {
            btnStart.Enabled = (bool)state;
        }

        private GrepServiceCallback _callback;
        private InstanceContext _instanceContext;
        private ServiceReference.DocumentGrepServiceClient _client;

        private void btnFiles_Click(object sender, EventArgs e)
        {
            using ( OpenFileDialog filesDlg = new OpenFileDialog() )
            {
                filesDlg.Filter = "Text files (*.txt)|*.txt|XML files (*.xml)|*.xml|HTML files (*.htm, *html)|*.htm;*html|All files (*.*)|*.*";
                filesDlg.FilterIndex = 4;
                filesDlg.Multiselect = true;

                if (filesDlg.ShowDialog() == DialogResult.OK)
                {
                    foreach (string path in filesDlg.FileNames)
                    {
                        AddSearchItem(path);
                    }
                }
            }
        }

        private void btnFolders_Click(object sender, EventArgs e)
        {
            using (FolderBrowserDialog folderDlg = new FolderBrowserDialog())
            {
                if (folderDlg.ShowDialog() == DialogResult.OK)
                {
                    foreach (string path in Directory.EnumerateFiles(folderDlg.SelectedPath))
                    {
                        AddSearchItem(path);
                    }
                }
            }
        }

        /// <summary>
        /// Resize handler to make path field stretchy
        /// </summary>
        /// <param name="sender">Not used</param>
        /// <param name="e">Not used</param>
        private void listSearchItems_Resize(object sender, EventArgs e)
        {
            if (listSearchItems.Width > colSearchCount.Width)
            {
                colDocumentPath.Width = listSearchItems.Width - colSearchCount.Width - 2*listSearchItems.Columns.Count;
            }
        }

        /// <summary>
        /// Enable/disable start button as needed
        /// </summary>
        /// <param name="sender">Not used</param>
        /// <param name="e">Not used</param>
        private void txtSearch_TextChanged(object sender, EventArgs e)
        {
            if (txtSearch.Text.Length > 0)
                btnStart.Enabled = true;
            else
                btnStart.Enabled = false;
        }

        /// <summary>
        /// Given a file path, adds that item to the list of documents to be searched.
        /// </summary>
        /// <param name="path">Path to a file.</param>
        private void AddSearchItem(string path)
        {
            if (!_pathToGuid.ContainsKey(path))
            {
                // not yet in list
                Guid guid = Guid.NewGuid();

                AddSearchItem(path, guid);
            }
        }

        private void AddSearchItem(string path, Guid guid)
        {
            if (!_guidToPath.ContainsKey(guid))
            {
                ListViewItem item = new ListViewItem(path);
                item.Tag = guid;
                listSearchItems.Items.Add(item);

                _guidToPath[guid] = path;
                _pathToGuid[path] = guid;
            }
        }

        private Dictionary<Guid, string> _guidToPath = new Dictionary<Guid, string>();
        private Dictionary<string, Guid> _pathToGuid = new Dictionary<string, Guid>(StringComparer.OrdinalIgnoreCase);

        private static void StartThreadProc(object state)
        {
            SearchStartArgs args = state as SearchStartArgs;

            foreach (KeyValuePair<string, Guid> pair in args._snapshot)
            {
                System.Diagnostics.Debug.WriteLine(string.Format("Client queuing {0} for processing.", pair.Key));

                if (args._isRegex)
                    args._iclient.QueueSearchByRegEx(pair.Key, args._searchText, pair.Value);
                else
                    args._iclient.QueueSearchByString(pair.Key, args._searchText, pair.Value);

                System.Diagnostics.Debug.WriteLine(string.Format("Client queued {0} for processing.", pair.Key));
            }

            args._uiContext.Post(args._form.PostEnableStartButton, true);
        }


        private void btnStart_Click(object sender, EventArgs e)
        {
            PostEnableStartButton(false);

            foreach (ListViewItem item in listSearchItems.Items)
            {
                if (item.SubItems.Count > 1)
                {
                    item.SubItems.Remove(item.SubItems[1]);
                }
            }

            // create a snapshot of the items
            SearchStartArgs args = new SearchStartArgs();
            args._snapshot = new Dictionary<string, Guid>(_pathToGuid);
            args._searchText = txtSearch.Text;
            args._isRegex = chkRegexSearch.Checked;
            args._iclient = _client;
            args._form = this;

            Thread startThread = new Thread(StartThreadProc);
            startThread.Start(args);
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnReset_Click(object sender, EventArgs e)
        {
            txtSearch.Text = "";
            chkRegexSearch.Checked = false;
            listSearchItems.Items.Clear();
            _guidToPath.Clear();
            _pathToGuid.Clear();
        }

        private void chkMonitorOnly_CheckedChanged(object sender, EventArgs e)
        {
            if (chkMonitorOnly.Checked)
            {
                btnReset_Click(sender, e);

                txtSearch.Enabled = false;
                chkRegexSearch.Enabled = false;
                btnFiles.Enabled = false;
                btnFolders.Enabled = false;
                btnStart.Enabled = false;
            }
            else
            {
                txtSearch.Enabled = true;
                chkRegexSearch.Enabled = true;
                btnFiles.Enabled = true;
                btnFolders.Enabled = true;
                txtSearch_TextChanged(sender, e);
            }
        }

        private void timerPing_Tick(object sender, EventArgs e)
        {
            try
            {
                _client.Ping();
            }
            catch(Exception)
            {
                // could try reconnection here
            }
        }
    }
}
