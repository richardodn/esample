﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace grepClientForms
{
    public class SearchResult
    {
        public SearchResult(string path, string searchText, Guid instance, int result)
        {
            Path = path;
            SearchText = searchText;
            Instance = instance;
            Result = result;
        }

        public string Path { get; set; }
        public string SearchText { get; set; }
        public Guid Instance { get; set; }
        public int Result { get; set; }
    }
}
