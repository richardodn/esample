﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace grepClientForms
{
    public class SearchStartArgs
    {
        public Dictionary<string, Guid> _snapshot;
        public string _searchText;
        public bool _isRegex;
        public ServiceReference.IDocumentGrepService _iclient;
        public SynchronizationContext _uiContext = SynchronizationContext.Current;
        public MainForm _form;
    }
}
