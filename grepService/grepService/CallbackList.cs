﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace grepService
{
    /// <summary>
    /// List of callback endpoints.
    /// This is a shared resource. You must obtain a lock on this before accessing.
    /// </summary>
    public class CallbackList : List<IDocumentGrepServiceCallback>
    {

    }
}
