﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using log4net;
using log4net.Config;

namespace grepService
{
    /// <summary>
    /// Class implementing the document grep service.
    /// </summary>
    [ServiceBehavior(
        ConcurrencyMode = ConcurrencyMode.Single,
        InstanceContextMode = InstanceContextMode.PerCall)]
    public class DocumentGrepService : IDocumentGrepService
    {
        /// <summary>
        /// Allows a client to subscribe to work results. A client must subscribe in order to be notified of call
        /// completions.
        /// Implementation of Subscribe method. Safely adds callback channel to list.
        /// </summary>
        public void Subscribe()
        {
            CreateCompletionThread();

            lock (_callbackList) // locks on anything derived from object
            {
                IDocumentGrepServiceCallback callback = OperationContext.Current.GetCallbackChannel<IDocumentGrepServiceCallback>();
                if (!_callbackList.Contains(callback))
                {
                    _callbackList.Add(callback);
                    Logger.Info("Subscriber added.");
                }
                else
                {
                    Logger.Info("Subscriber previously added.");
                }
            }

        }

        /// <summary>
        /// Unsubscribes from the work results.
        /// Implementation of Unsubscribe method. Safely removes callback channel from list.
        /// </summary>
        public void Unsubscribe()
        {
            lock (_callbackList) // locks on anything derived from object
            {
                IDocumentGrepServiceCallback callback = OperationContext.Current.GetCallbackChannel<IDocumentGrepServiceCallback>();
                if (_callbackList.Contains(callback))
                {
                    _callbackList.Remove(callback);
                    Logger.Info("Subscriber removed.");
                }
                else
                {
                    Logger.Info("Subscriber not present.");
                }
            }
        }

        /// <summary>
        /// Basic no-op call to keep the channel alive.
        /// </summary>
        public void Ping() { }

        /// <summary>
        /// Search for occurences of string "searchString" in document "documentPath." On completion, result is
        /// posted back to all subscribers through the IDocumentGrepCallback interface.
        /// </summary>
        /// <param name="documentPath">Full path to the document to be searched.</param>
        /// <param name="searchString">Search string.</param>
        /// <param name="clientInstance">Guid to identify the source of a request.</param>
        public void QueueSearchByString(string documentPath, string searchString, Guid clientInstance)
        {
            Logger.Info(string.Format("Queuing document \"{0}\" for standard text search on \"{1}\".", documentPath, searchString));

            // escape any special characters
            string regexString = Regex.Escape(searchString);

            // call implementation method
            DoQueue(documentPath, regexString, clientInstance);
        }

        /// <summary>
        /// Search for occurences of expression "regexString" in document "documentPath." On completion, result is
        /// posted back to all subscribers through the IDocumentGrepCallback interface.
        /// </summary>
        /// <param name="documentPath">Full path to the document to be searched.</param>
        /// <param name="regexString">Search expression.</param>
        /// <param name="clientInstance">Guid to identify the source of a request.</param>
        public void QueueSearchByRegEx(string documentPath, string regexString, Guid clientInstance)
        {
            Logger.Info(string.Format("Queuing document \"{0}\" for regular expression search on \"{1}\".", documentPath, regexString));

            // call implementation method
            DoQueue(documentPath, regexString, clientInstance);
        }

        /// <summary>
        /// Internal helper with common implementation
        /// </summary>
        /// <param name="documentPath">Full path to the document to be searched.</param>
        /// <param name="regexString">Search expression.</param>
        /// <param name="clientInstance">Guid to identify the source of a request.</param>
        private void DoQueue(string documentPath, string regexString, Guid clientInstance)
        {
            // Use the built in system managed thread pool to execute search operations
            ThreadPool.QueueUserWorkItem(SearchCallback, new SearchOperation(documentPath, regexString, clientInstance));
            Logger.Info(string.Format("Document \"{0}\" queued.", documentPath));
        }

        /// <summary>
        /// Method called by the thread pool to execute a search.
        /// </summary>
        /// <param name="state">SearchOperation object passed when work item enqueued</param>
        public static void SearchCallback(object state)
        {
            SearchOperation search = state as SearchOperation;

            Logger.Info(string.Format("{0} Performing grep operation for \"{1}\" using expression \"{2}\".", search.Instance.ToString(), search.Path, search.SearchText));
            DateTime start = DateTime.Now;
            
            int result = RegExGrep(search.Path, search.SearchText);
            
            TimeSpan elapsed = DateTime.Now.Subtract(start);
            Logger.Info(string.Format("{0} Elapsed time for search is {1} milliseconds. {2} occurences found.", search.Instance.ToString(), elapsed.TotalMilliseconds, result));

            lock(_resultQueue)
            {
                _resultQueue.Enqueue(new SearchResult(search, result));
            }

            Logger.Info(string.Format("{0} Result posted to queue.", search.Instance.ToString()));
        }

        /// <summary>
        /// On demand creation of the completion callback thread.
        /// </summary>
        private void CreateCompletionThread()
        {
            if (ReferenceEquals(_completionThread, null))
            {
                _completionThread = new Thread(CompletionThreadProc);
                _completionThread.Start();

                Logger.Debug("Completion thread constructed.");
            }
        }

        private Thread _completionThread;

        /// <summary>
        /// Method that watches for completions and sends notifications to subscribers
        /// </summary>
        private static void CompletionThreadProc()
        {
            WaitHandle [] handles = new WaitHandle[2];

            handles[0] = SearchResultQueue.CreateHasResultsWaitHandle();
            handles[1] = _exitSignalled;

            // loop breaks when _exitSignalled signalled
            while (true)
            {
                // wait for a handle to become signalled
                Logger.Debug("Waiting for signal.");
                int idx = WaitHandle.WaitAny(handles);

                if (idx == 1)
                {
                    // exit singalled
                    Logger.Debug("Exit signalled.");
                    break;
                }
                else // idx == 0
                {
                    // items in result queue, lock it for access
                    Logger.Debug("Result signalled.");
                    SearchResult result;
                    lock(_resultQueue)
                    {
                        Logger.Debug("Dequeue item.");
                        result = _resultQueue.Dequeue();
                    }

                    if (!ReferenceEquals(result, null))
                    {
                        Logger.Debug("Process item.");
                        // got a real object, notify subscribers
                        DateTime start = DateTime.Now;
                        lock (_callbackList)
                        {
                            Logger.Debug(string.Format("{0} Callback list locked.", result.Instance.ToString()));
                            CallbackList deadCallbacks = new CallbackList();

                            // loop over callbacks
                            foreach (IDocumentGrepServiceCallback callback in _callbackList)
                            {
                                try
                                {
                                    callback.OnSearchCompletedCallback(result.Path, result.SearchText, result.Instance, result.Result);
                                    Logger.Debug(string.Format("{0} Callback sent.", result.Instance.ToString()));
                                }
                                catch (Exception)
                                {
                                    // we really don't care what the exception was on the callback. If there is one, we simply
                                    // assume that process has died remove it from the list.
                                    deadCallbacks.Add(callback);
                                }
                            }

                            // Remove any that died
                            if (deadCallbacks.Count > 0)
                            {
                                foreach (IDocumentGrepServiceCallback callback in deadCallbacks)
                                {
                                    _callbackList.Remove(callback);
                                }
                            }
                        }

                        TimeSpan elapsed = DateTime.Now.Subtract(start);
                        Logger.Info(string.Format("{0} Elapsed time for subscriber notification is {1} milliseconds.", result.Instance.ToString(), elapsed.TotalMilliseconds));
                    }
                }
            }
        }

        /// <summary>
        /// Call this method when the host application is closing.
        /// </summary>
        public static void SignalAppExit()
        {
            _exitSignalled.Set();
        }

        private static ManualResetEvent _exitSignalled = new ManualResetEvent(false);

        /// <summary>
        /// List of clients maintaining callbacks.
        /// </summary>
        private static CallbackList _callbackList = new CallbackList();

        private static SearchResultQueue _resultQueue = new SearchResultQueue();

        /// <summary>
        /// Error codes returned as count < 0.
        /// </summary>
        public enum ErrorCodes
        {
            noError = 0,
            fileNotFound = -1,
        };

        /// <summary>
        /// Method that does the actual work of processing the document.
        /// </summary>
        /// <param name="documentPath">Path to document</param>
        /// <param name="regexString">regex search expression</param>
        /// <returns>Count of occurrences or an error value less than 0</returns>
        public static int RegExGrep(string documentPath, string regexString)
        {
            int count = (int)ErrorCodes.fileNotFound;

            if (File.Exists(documentPath))
            {
                long lineCount = 0;

                count = (int)ErrorCodes.noError;
                foreach (string line in File.ReadLines(documentPath))
                {
                    lineCount++;
                    count += Regex.Matches(line, regexString).Count;

                    if ((lineCount%100 == 0) && _exitSignalled.WaitOne(0))
                    {
                        // signalled
                        break;
                    }
                }
            }

            return count;
        }

        /// <summary>
        /// Provides a logging interface via log4net
        /// </summary>
        public static ILog Logger
        {
            get
            {
                if (ReferenceEquals(_logger, null))
                {
                    // create a log4net logger
                    _logger = LogManager.GetLogger(typeof(DocumentGrepService));

                    // and configure from settings in app.exe.cfg file
                    XmlConfigurator.Configure();
                }

                return _logger;
            }
        }

        private static ILog _logger;
    }
}
