﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ServiceModel;
using System.ServiceProcess;
using grepService;

namespace grepService
{
    public class DocumentGrepWinService : ServiceBase
    {
        private ServiceHost _serviceHost = null;
        public const string ServiceShortName = "DocGrepService";
        public const string ServiceDisplayName = "Document Grep Service";
        public const string ServiceDescription = "Provides grep services to clients.";

        public DocumentGrepWinService()
        {
            // Name the Windows Service
            ServiceName = ServiceShortName;
        }

        public static void Main()
        {
            ServiceBase.Run(new DocumentGrepWinService());
        }

        protected override void OnStart(string[] args)
        {
            if (_serviceHost != null)
            {
                _serviceHost.Close();
            }

            // Create a ServiceHost.
            _serviceHost = new ServiceHost(typeof(DocumentGrepService));

            // Open the ServiceHost to create listeners and start listening for messages.
            _serviceHost.Open();
        }

        protected override void OnStop()
        {
            if (_serviceHost != null)
            {
                DocumentGrepService.SignalAppExit();

                _serviceHost.Close();
                _serviceHost = null;
            }
        }

        // Note: Pause and Restart not supported
    }
}
