﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using System.ServiceProcess;
using System.Configuration;
using System.Configuration.Install;

namespace grepService
{
    [RunInstaller(true)]
    public class DocumentGrepWinServiceInstaller : Installer
    {
        private ServiceProcessInstaller process;
        private ServiceInstaller service;

        public DocumentGrepWinServiceInstaller()
        {
            process = new ServiceProcessInstaller();
            process.Account = ServiceAccount.LocalSystem;
            
            service = new ServiceInstaller();
            service.ServiceName = DocumentGrepWinService.ServiceShortName;
            service.DisplayName = DocumentGrepWinService.ServiceDisplayName;
            service.Description = DocumentGrepWinService.ServiceDescription;
            service.StartType = ServiceStartMode.Automatic;

            Installers.Add(process);
            Installers.Add(service);
        }
    }
}
