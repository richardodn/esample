﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace grepService
{
    /// <summary>
    /// Interface defining API entry points.
    /// </summary>
    [ServiceContract(
        SessionMode = SessionMode.Required,
        CallbackContract = typeof(IDocumentGrepServiceCallback),
        Namespace = "http://Odenweller.Services.DocumentGrep")]
    public interface IDocumentGrepService
    {
        /// <summary>
        /// Allows a client to subscribe to work results. A client must subscribe in order to be notified of call
        /// completions.
        /// </summary>
        [OperationContract]
        void Subscribe();

        /// <summary>
        /// Unsubscribes from the work results.
        /// </summary>
        [OperationContract]
        void Unsubscribe();

        /// <summary>
        /// Basic no-op call to keep the channel alive.
        /// </summary>
        [OperationContract]
        void Ping();

        /// <summary>
        /// Search for occurences of string "searchString" in document "documentPath." On completion, result is
        /// posted back to all subscribers through the IDocumentGrepCallback interface. Determination of document
        /// encoding is attempted at runtime.
        /// </summary>
        /// <param name="documentPath">Full path to the document to be searched.</param>
        /// <param name="searchString">Search string.</param>
        /// <param name="clientInstance">Guid to identify the source of a request.</param>
        [OperationContract(IsOneWay = true)]
        void QueueSearchByString(string documentPath, string searchString, Guid clientInstance);

        /// <summary>
        /// Search for occurences of expression "regexString" in document "documentPath." On completion, result is
        /// posted back to all subscribers through the IDocumentGrepCallback interface. Determination of document
        /// encoding is attempted at runtime.
        /// </summary>
        /// <param name="documentPath">Full path to the document to be searched.</param>
        /// <param name="regexString">Search expression.</param>
        /// <param name="clientInstance">Guid to identify the source of a request.</param>
        [OperationContract(IsOneWay = true)]
        void QueueSearchByRegEx(string documentPath, string regexString, Guid clientInstance);
    }

    /// <summary>
    /// Interface defining callback operations
    /// </summary>
    public interface IDocumentGrepServiceCallback
    {
        /// <summary>
        /// Callback to client applications when an search operation completes.
        /// </summary>
        /// <param name="documentPath">Full path to the document to be searched as originally provided by the caller.</param>
        /// <param name="searchString">Search string or expression as originally provided by the caller.</param>
        /// <param name="clientInstance">Guid to identify the source of a request as originally provided by the caller.</param>
        /// <param name="count">The count of occurences for search. Values less than 0 indicate an error. See XXX for error codes.</param>
        [OperationContract(IsOneWay=true)]
        void OnSearchCompletedCallback(string documentPath, string searchString, Guid clientInstance, int count);
    }
}
