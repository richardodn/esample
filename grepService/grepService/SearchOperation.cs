﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace grepService
{
    public class SearchOperation
    {
        public SearchOperation(string path, string search, Guid instance)
        {
            Path = path;
            SearchText = search;
            Instance = instance;
        }

        public string Path { get; set; }
        public string SearchText { get; set; }
        public Guid Instance { get; set; }
    }
}
