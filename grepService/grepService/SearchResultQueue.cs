﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
//using System.Threading.Tasks;

namespace grepService
{
    /// <summary>
    /// Queue collection of completed search operations.
    /// This is a shared resource. You must obtain a lock on this before accessing.
    /// </summary>
    public class SearchResultQueue
    {
        /// <summary>
        /// Adds the item to the queue and set the wait handle to signalled.
        /// </summary>
        /// <param name="result">The item to queue.</param>
        public void Enqueue(SearchResult result)
        {
            DocumentGrepService.Logger.Debug("Result enqueued.");
            // add to the queue
            _resultQueue.Enqueue(result);
            // set the event signalling items in queue
            DocumentGrepService.Logger.Debug("Setting _hasItems event.");
            _hasItems.Set();
        }

        /// <summary>
        /// Removes an item from the queue. If the queue is then empty, the wait handle is reset to unsignalled.
        /// </summary>
        /// <returns>The dequeued item.</returns>
        public SearchResult Dequeue()
        {
            SearchResult result = null;
            if (_resultQueue.Count > 0) // prevent thrown exception
            {
                DocumentGrepService.Logger.Debug("Result dequeued.");
                result = _resultQueue.Dequeue();
                if (_resultQueue.Count == 0)
                {
                    DocumentGrepService.Logger.Debug("Clearing _hasItems event.");
                    _hasItems.Reset();
                }
            }

            return result;
        }

        /// <summary>
        /// This method creates a named wait handle that becomes signalled when there are events in the queue.
        /// A thread that wishes to monitor and process this queue should:
        /// 1. Create a wait handle using this method.
        /// 2. Wait on the handle until signalled.
        /// 3. Once signalled, obtain a lock on this queue.
        /// 4. Dequeue an item. Release the queue.
        /// 5. Make sure it's not null as another thread may have processed the item(s). Do the work.
        /// 6. Go back to 2.
        /// </summary>
        /// <returns>A wait handle that becomes signalled when items are in the queue</returns>
        public static WaitHandle CreateHasResultsWaitHandle()
        {
            return new EventWaitHandle(false, EventResetMode.ManualReset, "SearchResultQueueHasItemsEvent");
        }

        private Queue<SearchResult> _resultQueue = new Queue<SearchResult>();
        private EventWaitHandle _hasItems = new EventWaitHandle(false, EventResetMode.ManualReset, "SearchResultQueueHasItemsEvent");
    }
}
