﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using grepService;
using Microsoft.VisualStudio.TestTools.UnitTesting;
namespace Test.grepService
{
    [TestClass()]
    public class DocumentGrepServiceTests
    {
        ///// <summary>
        /// Subscribe, Unsubscribe and Ping methods require a running service and WCF channel to test. Out of scope for now.
        /// </summary>
        //[TestMethod()]
        public void SubscribeTest()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Subscribe, Unsubscribe and Ping methods require a running service and WCF channel to test. Out of scope for now.
        /// </summary>
        //[TestMethod()]
        public void UnsubscribeTest()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Subscribe, Unsubscribe and Ping methods require a running service and WCF channel to test. Out of scope for now.
        /// </summary>
        //[TestMethod()]
        public void PingTest()
        {
            throw new NotImplementedException();
        }

        //[TestMethod()]
        public void QueueSearchByStringTest()
        {
            throw new NotImplementedException();
        }

        //[TestMethod()]
        public void QueueSearchByRegExTest()
        {
            throw new NotImplementedException();
        }

        //[TestMethod()]
        public void SearchCallbackTest()
        {
            throw new NotImplementedException();
        }

        //[TestMethod()]
        public void SignalAppExitTest()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Internally, all search operations are done using regular expressions.
        /// </summary>
        [TestMethod()]
        public void RegExGrepTest_Text()
        {
            // case sensitive search on "before"
            int count = DocumentGrepService.RegExGrep(@"..\..\..\TestData\eBooks\1jcfs10.txt", "before");
            Assert.AreEqual(count, 180);
        }

        /// <summary>
        /// Internally, all search operations are done using regular expressions.
        /// </summary>
        [TestMethod()]
        public void RegExGrepTest_Regex()
        {
            // case insensitive search on "before"
            int count = DocumentGrepService.RegExGrep(@"..\..\..\TestData\eBooks\1jcfs10.txt", "(?i)before");
            Assert.AreEqual(count, 203);
        }
    }
}
